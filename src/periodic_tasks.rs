use std::sync::Arc;

use crate::discord_handler::{get_merge_request_threads, VELOREN_SERVER_ID};
use serde_json::{from_str, Value};
use serenity::{prelude::*, utils::MessageBuilder};
use std::time::Duration;
use tokio::sync::RwLock;
use tracing::{error, log::info};

pub struct PeriodicTasker {
    pub client: reqwest::Client,
    pub gitlab_project_id: String,
    pub context: Arc<RwLock<Option<Context>>>,
}

impl PeriodicTasker {
    async fn close_threads(&self) -> Result<(), Box<dyn std::error::Error>> {
        let context = self.context.read().await;
        if let Some(context) = &*context {
            // Go through each thread and check if it's an MR thread
            let merge_requests =
                get_merge_request_threads(context.cache.guild(VELOREN_SERVER_ID).unwrap())?;

            for merge_request_number in merge_requests.keys() {
                let thread = merge_requests.get(merge_request_number).unwrap();

                // Skip any already archived threads
                if thread.thread_metadata.unwrap().archived {
                    continue;
                }

                // Query the GitLab API with the Veloren repo to find the MR
                let request = self
                    .client
                    .get(format!(
                        "https://gitlab.com/api/v4/projects/{}/merge_requests/{}",
                        self.gitlab_project_id, merge_request_number
                    ))
                    .build()?;
                let gitlab_response = self.client.execute(request).await?;
                let gitlab_response_body = gitlab_response.text().await?;
                let body_json: Value = from_str(&gitlab_response_body)?;

                // Close any threads that are not open. This will only hit threads that
                // specifically have `MR####` somewhere in their title.
                if body_json["state"] != "opened" {
                    // Log the thread name being closed
                    info!(
                        "Closing thread for MR {}: {} because it is no longer open",
                        merge_request_number, body_json["title"]
                    );
                    // Send a message to the thread that it is being closed
                    thread
                        .send_message(&context.http, |m| {
                            m.content(
                                MessageBuilder::new()
                                    .push("This thread is being archived as the MR it references is no longer in the 'open' state. Please message @AngelOnFira if this is causing undesirable behaviour!")
                                    .build(),
                            )
                        })
                        .await?;

                    // Close the thread
                    thread
                        .edit_thread(&context.http, |thread| thread.archived(true))
                        .await?;
                }
            }
        }

        Ok(())
    }

    pub async fn run(self) -> Result<(), Box<dyn std::error::Error>> {
        loop {
            {
                if let Err(e) = self.close_threads().await {
                    error!(?e, "Error while closing threads");
                    return Err(e);
                }
            }
            tokio::time::sleep(Duration::from_secs(5 * 60)).await;
        }
    }
}
